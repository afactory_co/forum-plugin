<?php namespace AfactoryCo\ForumPlugin\Channel\Command;

use Anomaly\ForumModule\Channel\Contract\ChannelRepositoryInterface;

class GetChannel
{

    /**
     * The channel slug.
     *
     * @var mixed
     */
    protected $slug;

    /**
     * Create a new GetChannel instance.
     *
     * @param $identifier
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Handle the command.
     *
     * @param  ChannelRepositoryInterface $channels
     * @return \Anomaly\ForumModule\Channel\ChannelModel|null
     */
    public function handle(ChannelRepositoryInterface $channels)
    {
        if (!$channel = $channels->findBySlug($this->slug)) {
            return null;
        }

        return $channel;
    }
}
