<?php namespace AfactoryCo\ForumPlugin\Channel\Command;

use Anomaly\ForumModule\Channel\Contract\ChannelRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\Streams\Platform\Support\Collection;
use Illuminate\View\Factory;

class RenderChannel
{

    use DispatchesJobs;

    /**
     * The rendering options.
     *
     * @var Collection
     */
    protected $options;

    /**
     * Create a new RenderNavigation instance.
     *
     * @param Collection $options
     */
    public function __construct(Collection $options)
    {
        $this->options = $options;
    }

    /**
     * Handle the command.
     *
     * @param  ChannelRepositoryInterface $channels
     * @return null|\Anomaly\ForumModule\Channel\Contract\ChannelInterface
     */
    public function handle(ChannelRepositoryInterface $channels, Factory $view)
    {
        $channel = $channels->findBySlug($this->options->get('slug'));

        return $view->make(
            $this->options->get('view', 'afactory_co.plugin.forum::channel/view'),
            [
                'channel'    => $channel,
                'options' => $this->options,
            ]
        )->render();
    }
}
