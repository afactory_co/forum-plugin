<?php

return [
    'title'       => 'Forum',
    'name'        => 'Forum Plugin',
    'description' => 'A simple forum widget plugin.'
];
